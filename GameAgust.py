from configAgust import CHOICES, WINNER, score
from random import choice
from decorator import time_log


def human_player():
    choice_input = input('enter your choice (p, r, s) : ')
    if choice_input and choice_input in CHOICES:
        return choice_input
    elif choice_input not in CHOICES:
        print('is not correct choice !! ')
        return human_player()


def system_player():
    system_choice = choice(CHOICES)
    return system_choice


def run(type_):
    if type_ == 's':
        player_1, player_2 = human_player(), system_player()
    else:
        player_1, player_2 = human_player(), human_player()
    return player_1, player_2


def winner(type_):
    p1, p2 = run(type_)
    choices = tuple(sorted((p1, p2)))
    if len(set(choices)) == 1:
        return 'd'
    else:
        if WINNER[choices] == p1:
            return 'p1'
        else:
            return 'p2'


def start(type_):
    score_set = {'player_1': 0, 'player_2': 0}
    while score_set['player_1'] < 3 and score_set['player_2'] < 3:
        win = winner(type_)

        if win == 'd':
            print('draw')
            print("#" * 24)

        elif win == 'p1':
            print('player one is win')
            print('\n')
            score_set['player_1'] += 1

        elif win == 'p2':
            print('player two is win')
            print("\n")
            score_set['player_2'] += 1

    if score_set['player_1'] == 3:
        print("@#$" * 7)
        print('player one is win ')
        print("#@$" * 7, "\n")
        score['player_1'] += 1
    elif score_set['player_2'] == 3:
        print("$#&" * 7)
        print("player two is win")
        print("*#@" * 7, "\n")
        score['player_2'] += 1

    print(" *_* " * 7)
    print('result in set : ')
    print(f'\tplayer 1 = {score_set["player_1"]}\n\tplayer 2 = {score_set["player_2"]}')
    print(f'score all set :\n\tplayer 1 = {score["player_1"]}\n\tplayer 2 = {score["player_2"]}')
    print(" *_* " * 7)

    again_input = input('\ndo you want play again ? (y or not again play click any key)')
    if again_input == 'y':
        start(type_)
    else:
        print('\nbye my friend, see you later\n')


def type_game():
    type_input = input('please choice game type. s for single player and m for multiplayer : ')
    if type_input and type_input in ('s', 'm'):
        print('type game are single player') if type_input == 's' else print('type game are multiplayer')
        return type_input
    else:
        print('this choice not exist')
        type_game()


@time_log
def game():

    start(type_game())


if __name__ == "__main__":
    game()
