CHOICES = ('p', 'r', 's')

WINNER = {
    ('p', 'r'): 'p',
    ('p', 's'): 's',
    ('r', 's'): 'r'
}

score = {'player_1': 0, 'player_2': 0}
