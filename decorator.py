from datetime import datetime


def time_log(func):
    def wrapped(*args, **kwargs):
        start = datetime.now()
        result = func(*args, **kwargs)
        end = datetime.now()
        total_time = end - start
        time = total_time.seconds
        print(f'total time : {int(time / 3600)}:{int(time / 60)}:{int(time % 60)}')

        return result

    return wrapped
