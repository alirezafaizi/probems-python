from string import ascii_letters, ascii_lowercase, ascii_uppercase, \
    digits, punctuation
from random import choices


def zarb():
    #  zarb 2 adad bedon estefade az amalgare zarb

    a = int(input('enter number one : '))
    b = int(input('enter number two : '))
    c = int()

    for _ in range(0, b):
        c += a
    print('answer = ', c)


def zoj_va_fard():
    #  zoj ya fard bodan yek adad

    number = input('enter your number : ')
    try:
        int(number)
    except ValueError:
        print('this is not a number !!!')
        zoj_va_fard()
    else:
        number = int(number)
        if number % 2 != 0:
            print('number is fard')
        else:
            print('number is zoj')


def min_and_max():
    #  hesab kardan bar haei ke be min va max reside

    my_list = [12, 10, 13, 9, 20, 7, 18, 14, 20]

    min = my_list[0]
    max = my_list[0]
    min_count = 0
    max_count = 0

    for i in my_list:
        if i < min:
            min_count += 1
            min = i  # 1 2 3
        if i > max:
            max_count += 1
            max = i  # 0 1 1

    print('min count : ', min_count)
    print('max count : ', max_count)


def baghimnade():
    #  neshan dadan baghi mande yek adad bedone estefade az amalgar taghsim

    number1 = input('enter your number1 : ')
    number2 = input('inter your number2 : ')
    try:
        number1, number2 = int(number1), int(number2)
    except ValueError:
        print('this is not number !!! your choice = ', number1, 'and\t', number2)
        baghimnade()
    else:
        if number1 < number2:
            print('must always number1 > number2')
            baghimnade()
        while number1 > number2:
            number1 = number1 - number2
        print(number1)


def pelekani():
    #  tashkhis pelekani bodan ya nabodane yek number

    number = input('enter your number : ')
    try:
        number = int(number)
    except ValueError:
        print('this is not a number!!! your choice = ', number)
        pelekani()
    else:
        my_list = list()
        my_bool = True

        while my_bool:
            if number % 10 >= 1:
                my_list.append(int(number % 10))
                number = int(number / 10)
            else:
                if number >= 1:
                    my_list.append(number)
                my_bool = False

        b = my_list[0]
        number_pelekani = True

        for i in my_list[1:]:
            if i - b == 1:
                b = i
            elif i - b == - 1:
                b = i
            else:
                print('this is not a number pelekani *_* ')
                number_pelekani = False
                break
        if number_pelekani:
            print('this number is a pelekani type')


def missing_number():
    #  peyda kardan number miss shode

    my_list = [1, 2, 3, 4, 5, 6, 8, 9, 10, 11]
    b = my_list[0]

    for i in my_list[1:]:
        if i - b == 1:
            b = i
        else:
            print('missing number is number ', b + 1)
            break


def factorial():
    #  bedast avardan factorial yek number

    number = input('enter your number : ')
    try:
        number = int(number)
    except ValueError:
        print('this is not a number my dear')
        factorial()
    else:
        list_number = [number]
        star = number
        my_bool = True
        while my_bool:
            if star - 1 > 0:
                star = star - 1
                list_number.append(star)
            else:
                my_bool = False

        answer = list_number[0]
        for i in list_number[1:]:
            answer = answer * i

        print(answer)

        print(list_number)


class CreatePassword:

    def create(self, length=8, letters=False, lower=False, upper=False,
                 digit=False, pun=False, default_all=False, dnt_choice= True):
        emp = ''

        if letters:
            emp += ascii_letters
            dnt_choice = False
        if lower:
            emp += ascii_lowercase
            dnt_choice = False
        if upper:
            emp += ascii_uppercase
            dnt_choice = False
        if digit:
            emp += digits
            dnt_choice = False
        if pun:
            emp += punctuation
            dnt_choice = False
        if default_all:
            emp += ascii_letters + digits + punctuation
            dnt_choice = False
        if dnt_choice:
            emp += ascii_letters

        return ''.join(choices(emp, k=length))


def stars():
    number_list = list(range(20, -1, -1))

    for i in range(20, 0, -1):
        print(" " * i, "*" * (number_list[i] + number_list[i]))

    number_list = list(range(20, -1, -1))
    for i in range(1, 21):
        print(" " * i, "*" * (number_list[i] + number_list[i]))


def show_detail_division():
    number = input('enter your number for division : ')
    divide = input('divide the number by how many ? ')
    try:
        number, divide = float(number), float(divide)
    except TypeError:
        print('please enter a number (integer, float or any type his number)')
        show_detail_division()
    else:
        num = list(divmod(number, divide))
        print(f'divisor = {int(num[0])} and left over {num[1]}')


def calculator():
    print("""
    to multiply : *
    to divide : /
    to collect : +
    to subtract : -
    to Exponentiation **""")
    number = input('enter math calculation : ')
    try:
        eval(number)
    except TypeError:
        print('please try again ')
        calculator()
    else:
        print(eval(number))


if __name__ == "__main__":
    # zoj_va_fard()
    # min_and_max()
    # baghimnade()
    # pelekani()
    # missing_number()
    # factorial()
    # create_password = CreatePassword()
    # password = create_password.create(length=50, upper=True, digit=True)
    # print(password)
    # stars()
    # show_detail_division()
    calculator()
